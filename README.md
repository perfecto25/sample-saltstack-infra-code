# SALTSTACK - Configuration Management, Remote Execution & Event Reactor
## Sample company Saltstack infrastructure code

### fixes
salt diff output from test=true

add

    line 246 of salt/output/highstate.py

    schanged, ctext = _format_changes(ret['changes'])
    + if not ctext and 'pchanges' in ret:
    +     schanged, ctext = _format_changes(ret['pchanges'])
    nchanges += 1 if schanged else 0


### check all visible file_root files

    salt-run fileserver.file_list saltenv=prod