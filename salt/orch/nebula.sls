## run
## salt-run state.orchestrate nebula saltenv=dev pillar='{"target": "host1"}'

{% set target = pillar.get("target") %}

# generate cert and key for target node on host1
orch_gencert_{{ target }}:
    salt.state:
        - tgt: "saltmaster"
        - pillar: {{ pillar }}
        - sls:
            - formula.nebula.gencert


# deploy nebula binary and certs to target
orch_nebula_cfg_{{ target }}:
    salt.state:
        - tgt: {{ target }}
        - pillar: {{ pillar }}
        - sls:
            - formula.nebula

