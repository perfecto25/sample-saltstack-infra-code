## specific per-node configurations
## CENTOS 7 LINUX
role: default
profile: development
env: prod
region: ch

# pillars
include:
    - data.ec2
    - data.blackout


# any additional formulas to apply to this host besides whatever is included in "data" pillars
formulas:
    - iptables
    - stunnel
    - redir
    - sudoers
    - backups.restic.os_s3 # backup OS to AWS S3 bucket using Restic

hosts:
    ## custom /etc/hosts entries
    - 123.12.11.10 host1
    - 55.44.33.22 host2

# interfaces on which to configure static routes
route_interfaces:
    - p1p1
    - p1p2

network_interfaces:
    - em1
    - p1p1
    - p1p2

audit:
    max_log_file: 200
    num_logs: 15
    cron: { hour: 16, minute: 05 }

monit:
    rules:
        - check network {{ grains.id }}-p1p1 with interface p1p1
        - check network {{ grains.id }}-p1p2 with interface p1p2
        - check process stunnel matching "/usr/bin/stunnel /etc/stunnel/stunnel.conf"
        - check process rsync-to-standy matching "/usr/bin/rsync host1*"
        - check process redir matching "/usr/sbin/redir"

filebeat:
    logs:
        - /home/user/logs/current/[io]*_logs/*/general.log  ## will match any log in path /home/user/logs/current/input_logs (or output_logs)/*/general.log
        - /home/user/logs/current/service/*/messaging_service.log
        - /home/user/logs/current/data/*.log


sudoers:
    - "jsmith ALL=(ALL) NOPASSWD: /bin/systemctl restart myservice"
