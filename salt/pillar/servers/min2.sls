## specific per-node configurations
## ROCKY 9 Linux
role: default
profile: openvpn
env: infra
region: aws_usa

# pillars
include:
    - common
    - data.ec2
    - data.openvpn

users:
    - kreaves
    - bwillis
    - sstallone

sshuttle:
    listen: 0.0.0.0
    monitor:  ## sshuttle will alert via email and Slack if the following hosts are unreachable
        - host1
        - host2
    relays:
        host1:
            - host3
            - host4
        host2:
            - host5 # can only get to host5 via host2

monit:
    rules:
        - check process ipsec_vpn matching "/usr/libexec/ipsec/pluto --leak-detective --config /etc/ipsec.conf --nofork"
        - check process sshuttle_host1 matching "/bin/python3.8 /usr/local/bin/sshuttle -r sshuttle@host1*"
        - check process sshuttle_host2 matching "/bin/python3.8 /usr/local/bin/sshuttle -r sshuttle@host2*"
