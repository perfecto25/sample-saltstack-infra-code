# PILLAR: Top

{% if saltenv == "base" %}

base:
  '*':
    - servers.{{ grains.id }}
    - ignore_missing: True

{% elif saltenv == "dev" %}

dev:
  '*':
    - servers.{{ grains.id }}
    - ignore_missing: True

{% endif %}