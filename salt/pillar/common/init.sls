# PILLAR - Common INIT

include:
    - common.groups
    - common.packages
    - common.sysctl

usergroups:
    - webadmins

users:
    - monit

formulas:
    - ssh
    - timezone.default
    - user.group
    - user
    - network.hosts
    - network.routes
    - network.interfaces
    - ssh
    - saltstack.minion
    - postfix
    - motd
    - crowdstrike
    - sudoers
    - crontab

cis:
    ignore:
        packages: ['cups']

sudoers:
    - "%webadmins ALL=(root) NOPASSWD: /usr/bin/ncdu -r *"
