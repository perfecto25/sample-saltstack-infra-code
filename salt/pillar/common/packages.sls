
# PILLAR: Common Packages

packages:
    - epel-release
    - tree
    - wget
    - unzip
    - rsync
    - pxz
    - lsof
    - htop
    - dmidecode
    - vim-enhanced
    - net-tools
    - glances
    - traceroute
    - bind-utils
    - mlocate
    - psmisc
    - iperf3
    - iftop
    - iotop
    - smem
    - jq
    - ncdu  # disk space usage analyzer
    - sysstat  # pidstat, iostat
    - p7zip
    - p7zip-plugins
    - tmux
    - fish
    - nmap-ncat
    {% if grains.osmajorrelease in [7,8] %}
    - mailx
    {% elif grains.osmajorrelease == 9 %}
    - s-nail
    {% endif %}

prohibited_packages:
    - telnet

disabled_services:
    - firewalld
