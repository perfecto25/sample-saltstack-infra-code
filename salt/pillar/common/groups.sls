# PILLAR: Common Groups

# service accts
groups:
    sysadmins: { gid: 15000 }
    users: { gid: 15001 }
    webadmins: { gid: 15002 }