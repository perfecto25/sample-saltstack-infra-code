formulas:
    - ssh.authenticator

packages:
    - google-authenticator

ssh:
    challenge_response_authentication: 'yes'
    custom_lines:
        - 'Match Group sysadmin'
        - '   AuthenticationMethods publickey,password publickey,keyboard-interactive'
