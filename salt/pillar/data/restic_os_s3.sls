# restic backups of OS to S3 bucket

formulas:
    - backups.restic.os_s3
    - crontab

crontab:
    root:
        restic OS backup to S3: "0 1 * * 3,6 /etc/restic/restic_os_s3.sh >> /var/log/restic_os_s3.log 2>&1"

s3_cred: {{ salt['sdb.get']('sdb://saltcred/restic_os_backup_s3') }}