formulas:
    - iptables
    - openvpn
    - monit
    - elastic.auditbeat
    - sshuttle
    - sshuttle.monitor
    - fail2ban

restic:
    include:
        - /opt/openvpn

ssh:
    allow_agent_forwarding: 'yes'
    allow_tcp_forwarding: 'yes'

