usergroups:
    - webadmins

users:
    - jsmith
    - bbryant

include:
    - common
    - data.ec2

formulas:
    - stunnel
    - monit
    - cron
    - iptables
    - yumcron
    - elastic.beats

packages:
    - certbot
    - python2-certbot-apache

sysctl:
    net.ipv4.ip_forward: 1
    net.ipv4.tcp_keepalive_intvl: 90
    net.ipv4.tcp_keepalive_probes: 11
    net.ipv4.tcp_keepalive_time: 600
    net.ipv4.conf.all.rp_filter: 0
    net.ipv4.conf.all.arp_filter: 0
    net.ipv4.conf.default.rp_filter: 0
    net.ipv4.conf.default.arp_filter: 0
    net.ipv4.conf.default.accept_source_route: 0
    kernel.sysrq: 0
    kernel.msgmax: 65536
    kernel.core_uses_pid: 1
    net.ipv4.tcp_syncookies: 1
    net.core.rmem_max: 36777216
    net.core.rmem_default: 3145728
    net.core.wmem_max: 8388608
    net.core.wmem_default: 65536
    net.ipv4.tcp_rmem: '4096 87380 8388608'
    net.ipv4.tcp_wmem: '4096 65536 8388608'
    net.ipv4.tcp_mem: '8388608 8388608 8388608'
    net.ipv4.tcp_low_latency: 1
    net.core.netdev_max_backlog: 30000
    vm.stat_interval: 3600
    vm.swappiness: 5
    vm.dirty_writeback_centisecs: 1500
    kernel.nmi_watchdog: 0

