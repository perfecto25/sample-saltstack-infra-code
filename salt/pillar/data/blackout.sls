## Minion Blackout - no production changes!!!

# production blackout
minion_blackout: False
minion_blackout_whitelist:
  - file.read
  - test.ping
  - drift.show
  - saltutil.sync_all
  - pillar.get
  - pillar.items
  - grains.get
  - grains.items
  - sudo.grant
  - sudo.revoke
  - sudo.status
  - state.sls