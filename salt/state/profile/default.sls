# STATE - Default Profile
include:
    # Additional Includes
    {% if salt['pillar.get']('formulas', False) %}
    {% for formula in salt['pillar.get']('formulas') %}
    - formula.{{ formula }}
    {% endfor %}
    {% endif %}

{% if salt['pillar.get']('exclude_formulas', False) %}
exclude:
    {% for formula in salt['pillar.get']('exclude_formulas') %}
    - sls: formula.{{ formula }}
    {% endfor %}
{% endif %}

#--------------------------------------------------------------

## Configure Rocky 9
{% if grains.osmajorrelease == 9 %}
enable_crb_repo:
    cmd.run:
        - name: dnf update -y; dnf upgrade --refresh -y; dnf config-manager --set-enabled crb
        - unless: dnf repolist | grep crb

start_NetworkManager:
    service.running:
        - name: NetworkManager
        - enable: True

{% endif %}

## install common packages
{% for package in salt['pillar.get']('packages') %}
{{ package }}:
    pkg.installed
{% endfor %}

## remove prohibited packages
{% for package in salt['pillar.get']('prohibited_packages') %}
{{ package }}:
    pkg.removed
{% endfor %}


## disable services
{% for svc in salt['pillar.get']('disabled_services') %}
{{ svc }}:
    service.dead:
        - enable: False
{% endfor %}

etc_issue:
    file.managed:
        - name: /etc/issue.net
        - source: salt://{{ slspath }}/files/etc_issue
        - user: root
        - group: root
        - mode: "0644"


# SYSCTL
{% if salt['pillar.get']('sysctl') %}
sysctl_dir:
    file.directory:
        - name: /etc/sysctl.d
        - makedirs: True
        - mode: "0755"
        - user: root
        - group: root

{% for sys_val in salt['pillar.get']('sysctl') %}
{{ sys_val }}:
    sysctl.present:
        - value: {{ salt['pillar.get']('sysctl:{}'.format(sys_val)) }}
{% endfor %}
{% endif %}

# restrict wheel group to 'su' command
su_command:
    file.replace:
        - name: /etc/pam.d/su
        - pattern: .*auth.*required.*pam_wheel.so.*use_uid
        - repl: auth    required    pam_wheel.so    use_uid
        - append_if_not_found: True

{% if grains.osmajorrelease in [7, 8, 9] %}
password_complexity:
    file.replace:
        - name: /etc/pam.d/system-auth
        - pattern: '^password.*requisite.*$'
        - repl: 'password    requisite     pam_pwquality.so try_first_pass local_users_only retry=3 authtok_type= minlen=8 lcredit=-1 dcredit=-1 dcredit=-1 ocredit=-1 enforce_for_root'
        - append_if_not_found: True
{% endif %}

root_email_forwarding:
    file.managed:
        - name: /root/.forward
        - source: salt://{{ slspath }}/files/forward
        - user: root
        - group: root
        - mode: "0644"

root_htop_config:
    file.managed:
        - name: /root/.config/htop/htoprc
        - makedirs: True
        - source: salt://{{ slspath }}/files/htoprc
        - user: root
        - group: root
        - mode: "0644"
