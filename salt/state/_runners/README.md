remove cached modules

    rm /var/cache/salt/master/extmods/runners/<runner name.py>

sync all custom runners

    salt-run saltutil.sync_all

restart salt master