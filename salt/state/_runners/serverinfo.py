# Server Information

## generates report on physical servers, creates a table of information and emails to user

import os
import time
import sys
import logging
from datetime import datetime, timedelta
from collections import OrderedDict
import jinja2
import socket

# Import the email modules
from email.mime.multipart import MIMEMultipart
from email.mime.text import MIMEText
from email.header import Header
from email.utils import formataddr
import smtplib

import salt
import salt.client
from salt.exceptions import AuthorizationError, SaltClientError

client = salt.client.LocalClient()

log = logging.getLogger(__name__)

current_time = datetime.now()
base_dir = "/srv/saltstack/prod/salt/state/_runners/"
from_addr = "watcher@company.com"
to_addr = "devops@company.com"
mailhost = "YOUR MAIL HOST"


def _send_email(to_addr, from_addr, cc=None, bcc=None, subject=None, body=None):

    if not to_addr or not from_addr:
        print("error sending email, To or From values are null")
        log.error("error sending email, To or From values are null")
        return "error"

    # convert TO into list if string
    if type(to_addr) is not list:
        to_addr = to_addr.split()

    to_list = to_addr + [cc] + [bcc]
    to_list = [i for i in to_list if i]  # remove null emails

    msg = MIMEMultipart()
    msg["From"] = from_addr
    msg["Subject"] = subject
    msg["To"] = ",".join(to_addr)
    msg["Cc"] = cc
    msg["Bcc"] = bcc

    msg.attach(MIMEText(body, "html"))
    try:
        server = smtplib.SMTP(mailhost)
    except smtplib.SMTPAuthenticationError as e:
        log.error("Error authetnicating to SMTP server: %s, exiting.., %s" % (str(e)))
        return "error"
    except socket.timeout:
        log.error("SMTP login timeout")
        return "error"

    try:
        server.sendmail(from_addr, to_list, msg.as_string())
    except smtplib.SMTPException as e:
        log.error("Error sending email")
        log.error(str(e))
    finally:
        server.quit()


def _render_template(template, **kwargs):
    """renders a Jinja template into HTML"""
    # check if template exists
    if not os.path.exists(template):
        log.error("No template file present: %s" % template)
        return "error"

    templateLoader = jinja2.FileSystemLoader(searchpath="/")
    templateEnv = jinja2.Environment(loader=templateLoader)
    templ = templateEnv.get_template(template)
    return templ.render(**kwargs)


def _generate_report():
    """create Info report"""

    ret = {}

    ## get only Physical servers
    minions = client.cmd("virtual:physical", "test.ping", timeout=3, tgt_type="grain")
    minion_list = []
    for key, val in minions.items():
        if val is True:
            minion_list.append(key)

    if not minion_list:
        log.error("no minions available")
        sys.exit(1)

    for minion in sorted(minion_list):

        ret[minion] = {}
        ret[minion] = client.cmd(minion, "grains.items")[minion]
        ret[minion]["isolcpus"] = client.cmd(minion, "cmd.run", ["cat /sys/devices/system/cpu/isolated"])[minion]
        ret[minion]["disks"] = client.cmd(minion, "disk.usage")[minion]
        ret[minion]["ht"] = client.cmd(minion, "cmd.run", ["cat /sys/devices/system/cpu/smt/active"])[minion]

    ret = OrderedDict(sorted(ret.items(), key=lambda x: x[0]))
    date = time.strftime("%Y-%m-%d %H:%M:%S", time.localtime())

    html = _render_template(base_dir + "/serverinfo.j2", ret=ret, date=date)

    subject = "Server Info - Physical Servers (%s)" % time.strftime("%m/%d/%Y", time.localtime())

    try:
        _send_email(to_addr, from_addr, cc=None, bcc=None, subject=subject, body=html)
    except Exception as e:
        log.error(str(e))
    log.warning("report has been generated and emailed to %s" % to_addr)


def report():
    log.warning("generating Server info")
    _generate_report()
