from __future__ import absolute_import, print_function

# , unicode_literals

### Certbot renewer
### requires boto3, awscli module (pip install boto3, awscli)

### Run from salt master:  salt-run certbot.renew web1

import logging
import boto3
import requests
from collections import defaultdict

from requests.packages.urllib3.exceptions import InsecureRequestWarning

requests.packages.urllib3.disable_warnings(InsecureRequestWarning)
from dictor import dictor
import salt.client
from salt.exceptions import AuthorizationError

log = logging.getLogger(__name__)

webservers = ["web1", "web2", "web3"]
sg_web = "sg-<YOUR SG ID>"  # port 80,443 open SG - Secureity Group ID that has 80,443 open to public


def renew(tgt=None):
    """
    renews Certbot SSL certificates for websites
    """

    if tgt not in webservers:
        return "target server is not a webserver"

    ec2 = boto3.resource("ec2")

    for instance in ec2.instances.all():
        taglist = instance.tags
        for tag in taglist:
            if tag["Value"] == tgt:

                all_sg_ids = [
                    sg["GroupId"] for sg in instance.security_groups
                ]  # Get a list of ids of all securify groups attached to the instance

                # add webports
                all_sg_ids.append(sg_web)

                try:
                    instance.modify_attribute(Groups=all_sg_ids)
                except Exception as excep:
                    return "cannot modify target AWS security group"

                # add iptables 80,443 open
                cmd = "iptables -I INPUT 1 -p tcp -m multiport --dports 80,443 -j ACCEPT"
                try:
                    __salt__["salt.execute"](tgt, "cmd.run", arg=[cmd])
                except Exception as excep:
                    return str(excep)

                # renew certs
                cmd = "certbot renew"
                try:
                    __salt__["salt.execute"](tgt, "cmd.run", arg=[cmd])
                except Exception as excep:
                    return str(excep)

                # remove iptables rule
                cmd = "iptables -D INPUT 1"
                try:
                    __salt__["salt.execute"](tgt, "cmd.run", arg=[cmd])
                except Exception as excep:
                    return str(excep)

                # remove SG group
                all_sg_ids.remove(sg_web)

                try:
                    instance.modify_attribute(Groups=all_sg_ids)
                except Exception as excep:
                    return str(excep)

                return "certbot certs updated"


def show(tgt):
    try:
        return __salt__["salt.execute"](tgt, "cmd.run", arg=["certbot certificates"])
    except Exception as excep:
        return str(excep)
