#!/usr/bin/python3
import socket
import logging
from salt.utils.platform import is_linux

log = logging.getLogger(__name__)

# set networks
networks = {}
networks["network_a"] = ["host1", "host2", "host3"]
networks["network_b"] = [
    "hostA",
    "hostB",
    "hostC",
    "hostD",
]
networks["network_c"] = ["host5", "host6"]


def get_network():
    """determines what My Network a host is in"""

    if not is_linux():
        return

    hostname = socket.gethostname()
    content = open("/sys/devices/virtual/dmi/id/product_serial")
    serial = content.read()
    network = ""
    for nw in networks:
        if hostname in networks[nw]:
            network = nw
            # ret['company_network'] = nw
        elif serial.startswith("ec2"):
            # ret['company_network'] = 'ec2'
            network = "ec2"

    if not network:
        network = "unknown"

    return {"my_network": network}


if __name__ == "__main__":
    print(get_network())
