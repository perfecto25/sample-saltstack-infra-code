# STATE: Top

{% if saltenv == "base" %}

base:
  '*':
    - role.{{ salt['pillar.get']('role', 'default') }}

{% elif saltenv == "dev" %}

dev:
  '*':
    - role.{{ salt['pillar.get']('role', 'default') }}

{% endif %}