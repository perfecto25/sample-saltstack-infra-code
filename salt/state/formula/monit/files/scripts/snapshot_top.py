#!/usr/bin/python
from __future__ import print_function
import os
import sys
import subprocess
import time
import platform
from subprocess import Popen, PIPE
from utils import send_email, render_template, log

from_addr = "monit@company.com"
to_addr = "to@company.com"
# smtp_server = 'localhost'

base_dir = "/etc/monit/scripts/"
hostname = os.uname()[1]


def top_old():
    """gets top results for systems with older python version"""
    import commands
    import re

    r = re.compile("^\d{2}$")
    cmd = "top -cbn1"
    output = commands.getoutput(cmd).split("\n")
    top_header = ""

    for i in range(0, 5):
        top_header += output[i] + "\n"

    cmd = "ps -eo pid,user,ni,thcount,%mem,rss,vsz,%cpu,cputime,s,args --sort=-rss"
    output = commands.getoutput(cmd).split("\n")
    ps_header = output[0].split()
    del output[0]
    for i in range(0, len(output)):
        tmp = output[i].split()
        tmp[5] = str(int(tmp[5]) / 1024) + "m"
        tmp[6] = str(int(tmp[6]) / 1024) + "m"
        output[i] = tmp[0:10]
        started = ""
        val = ""
        tmp_cmd = ""
        if len(tmp) > 10:
            val = tmp[10]
            m = r.match(val)
            if m:
                started = started + " " + val
                del tmp[10]
            tmp_cmd = " ".join(tmp[10 : len(tmp)])
        output[i].append(started)
        output[i].append(tmp_cmd)
    output.insert(0, ps_header)
    today = time.strftime("%Y-%m-%d %H:%M:%S", time.localtime())
    msg = (
        hostname
        + "<br><br>"
        + today
        + "<br><br>"
        + top_header.replace("\n", "<br>")
        + "<br><br>"
        + "<TABLE BORDER=1 width=800>"
    )
    for l in output:
        msg += "<TR>"
        for i in l:
            msg += "<TD>" + i + "</TD>"
        msg += "</TR>"
    msg += "</TABLE>"
    return msg


def top():
    """creates a Dictionary of a TOP snapshot"""

    import psutil

    cmd = "ps -eo pid,user,ni,rss,vsz,cputime,lstart,etimes,time,%cpu,%mem,args --sort=-rss | head -n 25"

    try:
        result = subprocess.Popen(cmd, shell=True, stdout=subprocess.PIPE)
    except subprocess.CalledProcessError as e:
        log.error(e.output)

    time.sleep(2)  # wait for snapshot to be generated

    result = result.stdout.read().split("\n")  # get top 25 procs
    result = filter(None, result)  # remove empty rows
    title = result[0].split()  # get Title bar
    result.pop(0)

    allprocs = result

    procs = []  # Procs Dictionary for top commands

    for pr in allprocs:
        procs.append(pr.split())

    snapshot = {}
    snapshot["title"] = title
    snapshot["procs"] = procs

    # get memory & swap
    snapshot["mem"] = {}
    snapshot["swap"] = {}
    vmem = psutil.virtual_memory()
    swap = psutil.swap_memory()

    # get memory in GB format
    snapshot["mem"]["total"] = float("{0:.2f}".format(vmem.total / (1024.0 ** 3)))
    snapshot["mem"]["avail"] = float("{0:.2f}".format(vmem.available / (1024.0 ** 3)))
    snapshot["mem"]["free"] = float("{0:.2f}".format(vmem.free / (1024.0 ** 3)))
    snapshot["mem"]["used"] = float("{0:.2f}".format(vmem.used / (1024.0 ** 3)))

    # get swap in GB format
    snapshot["swap"]["total"] = float("{0:.2f}".format(swap.total / (1024.0 ** 3)))
    snapshot["swap"]["used"] = float("{0:.2f}".format(swap.used / (1024.0 ** 3)))
    snapshot["swap"]["free"] = float("{0:.2f}".format(swap.free / (1024.0 ** 3)))

    return snapshot


def email(html):
    subject = "Monit (%s): Top Snapshot" % hostname
    try:
        log.info('sending Monit "top memory" snapshot')
        send_email(to_addr, from_addr, cc=None, bcc=None, subject=subject, body=html)
    except Exception as e:
        log.error(str(e))


if __name__ == "__main__":
    date = time.strftime("%Y-%m-%d %H:%M:%S", time.localtime())

    if platform.python_version() < "2.7.5":
        email(top_old())
    else:
        html = render_template(base_dir + "/j2/top.j2", result=top(), hostname=hostname, date=date)
        email(html)
