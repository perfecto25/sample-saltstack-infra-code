#!/bin/bash

MAXMSG=10
MSG=$( postqueue -p | egrep '\-\- [0-9]+ Kbytes in [0-9]+ Request[s]*\.' | awk '{ print $5 }'  )
[ ${MSG:-0} -le $MAXMSG ] && exit 0 || exit 1
