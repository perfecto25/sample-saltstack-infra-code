## MMonit Console Server

mmonit_systemd:
    file.managed:
        - name: /etc/systemd/system/mmonit.service
        - source: salt://{{ slspath }}/files/mmonit.systemd
        - user: root
        - group: root
        - mode: 440

mmonit_server_xml:
    file.managed:
        - name: /opt/mmonit/conf/server.xml
        - source: salt://{{ slspath }}/files/server.xml
        - user: root
        - group: root
        - mode: 644

mmonit_license:
    file.managed:
        - name: /opt/mmonit/conf/license.xml
        - source: salt://{{ slspath }}/files/license.xml
        - user: root
        - group: root
        - mode: 644