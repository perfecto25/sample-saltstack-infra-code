# MONIT monitoring agent

- Monit agent is installed on each company host.

- Monit baseline config is placed into /etc/monit/monit.conf (Read-only for monit user)

- additional per-server config is placed into /etc/monit/company.conf

## Regenerate Monit ID

- Stop monit on the host with the duplicate ID

- Change the Monit ID. If you use Monit 5.8 or newer, use monit -r to reset the ID. For older Monit versions just remove the ID file. For example: rm -f ~/.monit.id (the location can have been changed with the "set idfile" statement in .monitrc),

- Start monit (it will automatically generate a new ID file)

## Email & Alerts
Email is sent via M/Monit (web UI) located on host1 server

To configure emails, login as admin


    http://host1:19840/admin/alerts


on host1, add firewall rule to allow agent to connect, 

vi /etc/sysconfig/iptables, add line

    -A INPUT -s <IP of host>/32 -p tcp -m tcp --dport 19840 -m state --state NEW,ESTABLISHED -j ACCEPT


## To check Monit status on a box

    monit validate -c /etc/monit/monit.conf 


## check Monit config file syntax

    monit -t -c monit.conf


## fix Monit SSH login issue

    monit@server> restorecon -R .ssh (and exit)
    

## Monit Server (Console)
download mmonit tar gz from website, 
    
    mv mmonit-3.7.6-linux-x64.tar.gz /opt
    cd /opt && tar -xvzf mmonit-3.7.6-linux-x64.tar.gz
    mv mmonit-3.7.6 mmonit
    
install Postgres SQL
    
    cd /opt/mmonit-3.7.6/db
    
    createuser -U postgres -P <pw>
    createdb -U postgres -E utf8 -O mmonit <pw>
    psql -U mmonit <pw> < mmonit-schema.postgresql
    
configure Systemd file (server.sls)

    systemctl daemon-reload
    systemctl enable mmonit
    systemctl start mmonit

default login

    admin
    swordfish
    
Upgrade from previous
    
    # copy previous mmonit install to /opt
    /opt/mmonit-3.6.1
    
    cd /opt/mmonit/upgrade
    ./upgrade -p /opt/mmonit-3.6.1

    # this will copy DB and config from older version
    
Migrate SQLite DB to Postgres

    cd db
    ./migrate_db.sh -t postgresql | psql -U mmonit mmonit
    
Run monit.server state to update config 