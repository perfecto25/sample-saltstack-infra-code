# STATE - Monit monitoring

monit_package:
  pkg.installed:
    - name: monit
    - version: 5.30.0-1.el{{ grains.osmajorrelease }}

monit_user:
    group.present:
        - name: monit
        - gid: 2008
    user.present:
        - name: monit
        - uid: 2008
        - gid: 2008
        - shell: /bin/bash
        - allow_uid_change: True
        - groups:
            - monit

monit_dir:
    file.directory:
        - name: /etc/monit
        - mode: 0755
        - user: monit
        - group: monit

monit_scripts:
    file.recurse:
        - name: /etc/monit/scripts
        - source: salt://{{ slspath }}/files/scripts
        - makedirs: True
        - user: monit
        - group: monit
        - file_mode: 0700

monit_configs_dir:
    file.directory:
        - name: /etc/monit/configs
        - makedirs: True
        - user: monit
        - group: companyusers
        - mode: 0774

monit_config:
    file.managed:
        - name: /etc/monit/monit.conf
        - source:
            - salt://{{ slspath }}/files/configs/host/{{ grains.id }}.j2
            - salt://{{ slspath }}/files/configs/profile/{{ salt['pillar.get']('profile') }}.j2
            - salt://{{ slspath }}/files/configs/default.j2
        - template: jinja
        - makedirs: True
        - mode: 0400
        - user: monit
        - group: monit

create_symlink:
    file.symlink:
        - name: /home/monit/.monitrc
        - target: /etc/monit/monit.conf

monit_log:
    cmd.run:
        - name: touch /var/log/monit.log
        - onlyif: "[ ! -f /var/log/monit.log ]"

    file.managed:
        - name: /var/log/monit.log
        - user: monit
        - group: monit
        - mode: 0640

monit_logrotate:
    file.managed:
        - name: /etc/logrotate.d/monit
        - source: salt://{{ slspath }}/files/logrotate
        - user: root
        - group: root
        - mode: 440
# set RW access to Sylog for monit user (OOM monitoring)
acl_messages:
    acl.present:
        - name: /var/log/messages
        - acl_type: user
        - acl_name: monit
        - perms: rx

monit_systemd:
    file.managed:
        - name: /etc/systemd/system/monit.service
        - source: salt://{{ slspath }}/files/monit.systemd
        - user: root
        - group: root
        - mode: 644

monit_service:
    service.running:
        - name: monit
        - enable: True

monit_sudoers:
    file.managed:
        - name: /etc/sudoers.d/monit
        - source: salt://{{ slspath }}/files/monit_sudo
        - user: root
        - group: root
        - mode: 440

reload_config:
    cmd.wait:
        - name: monit reload
        - runas: monit
        - watch:
            - file: monit_config

# cleanup RPM files
/etc/monitrc:
    file.absent: []

/etc/monit.d:
    file.absent: []

