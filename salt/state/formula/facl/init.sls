{% set facl = salt['pillar.get']('facl') %}
{% for list in facl %}
setfacl_{{ list }}:
    acl.present:
        - name: {{ list }}
        - acl_type: {{ facl[list]['type'] }}
        - acl_name: {{ facl[list]['name'] }}
        - perms: {{ facl[list]['perm'] }}
        - recurse: {{ facl[list]['recurse'] }}
{% endfor %}
