# Linux ACL Permissions

check permissions on directory

    getfacl /path/to/dir

remove all ACLs
    
    setfacl -b /path/to/dir

give Sysadmins group 777 to /opt/test

    setfacl -m group:admins:rwx /opt/test

to set recursively down,

    setfacl -Rm u:joe:rwx /home/mary

remove ACL

    setfacl -x user:antony /opt/test

give r/w access to /home/user1 and preserve SSH security
    
    chmod 750 /home/user1
    setfacl -m user:user2:rw /home/user1

remove all ACLs from file or dir

    setfacl -b /home/user1

set a default ACL for a directory (all new files or dirs created in this directory will inherit ACL permissions)
    
    setfacl -d -m u::rwx,g::rwx,o::r- /opt/testdir