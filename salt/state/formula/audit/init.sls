audit_config:
    file.managed:
        - name: /etc/audit/auditd.conf
        - source: salt://{{ slspath }}/files/auditd.conf.j2
        - template: jinja
        - user: root
        - group: root
        - mode: 640
            
audit_rules:
    file.managed:
        - name: /etc/audit/rules.d/audit.rules
        - source: salt://{{ slspath }}/files/audit.rules
        - user: root
        - group: root
        - mode: 600

auditd_service:
    cmd.run:
        - name: service auditd restart
        - onchanges:
            - file: /etc/audit/rules.d/audit.rules
    service.running:
        - name: auditd
        - enable: True

/mnt/hc/audit:
    file.directory:
        - dir_mode: 0700
        - user: root
        - group: root
        - makedirs: True

compress_script:
    file.managed:
        - name: /etc/audit/log_compress.sh
        - source: salt://{{ slspath }}/files/log_compress.sh.j2
        - template: jinja
        - user: root
        - group: root
        - mode: 700

compress_old_logs:
    cron.present:
        - name: "/etc/audit/log_compress.sh > /dev/null 2>&1"
        - user: root
        - hour: {{ salt['pillar.get']('audit:cron:hour', '1') }}
        - minute: {{ salt['pillar.get']('audit:cron:minute', '0') }}
        - dayweek: 1-5

mv_audit_logs_to_NAS:
    cron.present:
        - identifier: mv_audit_logs_to_NAS
        - name: /bin/rsync -azP --remove-source-files /mnt/disk/audit/*.xz admin@nas:/v1/data/audit_logs/{{ grains.id }}
        - user: root
        - hour: 22
        - minute: 30
        - daymonth: 15