## SUDOERS
add_company_sudoers:
    file.managed:
        - name: /etc/sudoers.d/company_sudo
        - source: salt://{{ slspath }}/files/company_sudo.j2
        - user: root
        - group: root
        - template: jinja
        - mode: 440