# STATE - Kapacitor

kapacitor_yum_repo:
    file.managed:
        - name: /etc/yum.repos.d/influxdb.repo
        - source: salt://{{ slspath }}/files/influxdb.repo
        - user: root
        - group: root
        - mode: 644

kapacitor_pkg:
    pkg.installed:
        - name: kapacitor

kapacitor_config:
    file.managed:
        - name: /etc/kapacitor/kapacitor.conf
        - source: salt://{{ slspath }}/files/kapacitor.conf
        - user: root
        - group: root
        - require:
            - pkg: kapacitor_pkg
 
kapacitor_service:
    service.running:
        - name: kapacitor
        - enable: True
        - watch:
            - file: kapacitor_config
