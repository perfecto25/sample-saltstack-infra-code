# TICK Stack

once InfluxDB is installed, create a new admin user

1. install InfluxDB

        state.sls formula.influxdb.server


2. create Admin user

        /usr/bin/influx -execute "CREATE USER admin WITH PASSWORD 'companymetrics' WITH ALL PRIVILEGES"


3. install Telegraf


        state.sls formula.influxdb.telegraf


4. install Grafana


        state.sls formula.grafana.server


### access grafana
http://HOSTIP:3000 
