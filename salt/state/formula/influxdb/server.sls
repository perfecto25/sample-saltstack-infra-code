# STATE - InfluxDB Server


influxdb_yum_repo:
    file.managed:
        - name: /etc/yum.repos.d/influxdb.repo
        - source: salt://{{ slspath }}/files/influxdb.repo
        - user: root
        - group: root
        - mode: 644

install_influx:
    pkg.installed:
        - name: influxdb
    service.running:
        - name: influxdb
        - enable: True

influxdb_config:
    file.managed:
        - name: /etc/influxdb/influxdb.conf
        - source: salt://{{ slspath }}/files/influxdb.conf
        - user: root
        - group: root
        - require:
            - pkg: influxdb

influxdb_service:
    service.running:
        - name: influxdb
        - enable: True
        - watch:
            - file: influxdb_config

        
