# STATE - Chronograf

{% set name = 'chronograf-1.6.1.x86_64.rpm' %}

chronograf_pkg:
    file.managed:
        - name: /tmp/{{ name }}
        - source: https://dl.influxdata.com/chronograf/releases/{{ name }}
        - source_hash: sha256=3dc8978fc324640a49aac46d1a90a1268b3ca9b4cbf7ad739d57685cae3b0789
        - unless: test -f /tmp/{{ name }}

chronograf_install:
    pkg.installed:
        - sources:
            - chronograf: /tmp/{{ name }}
        - require:
            - file: chronograf_pkg
        - watch:
            - file: chronograf_pkg

chronograf_init:
    file.managed:
        - name: /etc/init.d/chronograf
        - source: salt://{{ slspath }}/files/chronograf.init
        - mode: 555
        - template: jinja


{# chronograf_config:
    file.managed:
        - name: /etc/default/chronograf
        - source: salt://influxdb/files/chronograf_env.sh
        - mode: 644
        - context:
            chronograf: {{ chronograf }}
        - template: jinja #}