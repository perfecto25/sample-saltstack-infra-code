# STATE - Telegraf Collection Agent

{# telegraf_yum_repo:
    file.managed:
        - name: /etc/yum.repos.d/influxdb.repo
        - source: salt://formula/influxdb/files/influxdb.repo
        - user: root
        - group: root
        - mode: 644 #}

telegraf_pkg:
    pkg.installed:
        - name: telegraf

telegraf_config:
    file.managed:
        - name: /etc/telegraf/telegraf.conf
        - source: salt://{{ slspath }}/files/telegraf.conf.j2
        - template: jinja
        - user: root
        - group: root
        - require:
            - pkg: telegraf_pkg

log_dir:
    file.directory:
        - name: /var/log/telegraf
        - user: telegraf
        - group: telegraf
        - makedirs: True
        - mode: 755

telegraf_service:
    service.running:
        - name: telegraf
        - enable: True
        - watch:
            - file: telegraf_config
