#!/bin/bash

# show mod version of each mod
#set -x

for mod in $(lsmod | awk '{print $1}')
do
    [ $mod == "Module" ] && continue
    rhelversion=$(modinfo $mod | grep rhelversion | sed 's/rhelversion: //' | tr -d '[:space:]')
    srcversion=$(modinfo $mod | grep srcversion | sed 's/srcversion: //' | tr -d '[:space:]')
    version=$(modinfo $mod | grep ^version: | sed 's/version: //' | tr -d '[:space:]')
    echo -e "[$mod]"
    echo -e "version=$version"
    echo -e "rhelversion=$rhelversion"
    echo -e "srcversion=$srcversion"
    echo -e "\n"
done
