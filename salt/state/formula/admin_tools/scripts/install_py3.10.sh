#!/bin/bash
# Installs Python 3.10 on Centos 7 

yum -y install libffi-devel gcc openssl-devel bzip2-devel wget

cd /tmp

wget https://www.python.org/ftp/python/3.10.0/Python-3.10.0.tgz
tar xzf Python-3.10.0.tgz
cd Python-3.10.0/
./configure --enable-optimizations
make altinstall

[ -f /usr/bin/python3.10 ] || ln -s /usr/local/bin/python3.10 /usr/bin/python3.10

rm -f /tmp/Python-*.tgz
rm -rf /tmp/Python-*