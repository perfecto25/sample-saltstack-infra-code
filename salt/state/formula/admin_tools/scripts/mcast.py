#!/usr/bin/env python
## Checks for Multicast connectivity
## usage: ./mcast.py <group IP> <port> <iface IP>
## ./mcast.py 226.22.11.1 15600 192.168.38.21

import socket;
import struct;
import commands;
import sys;

if len(sys.argv) > 1:
        if sys.argv[1] is not None:
                group = sys.argv[1];
if len(sys.argv) > 2:
        if sys.argv[2] is not None:
                port = int(sys.argv[2]);
if len(sys.argv) > 3:
        if sys.argv[3] is not None:
                interface = sys.argv[3];
#else:
#               interface = str(0)

try:
        group;
except NameError:
        group = raw_input("Multicast Channel: ");
try:
        port;
except NameError:
        port = input("Multicast Port: ");
try:
        interface;
except NameError:
        interface = raw_input("Interface Address(Blank for Default): ");

sock = socket.socket(socket.AF_INET, socket.SOCK_DGRAM, socket.IPPROTO_UDP);
sock.setsockopt(socket.SOL_SOCKET, socket.SO_REUSEADDR, 1);
sock.bind((group, port));

if interface is "":
        mreq = struct.pack('4sl', socket.inet_aton(group), socket.INADDR_ANY);
else:
        mreq = socket.inet_aton(group) + socket.inet_aton(interface);

sock.setsockopt(socket.IPPROTO_IP, socket.IP_ADD_MEMBERSHIP, mreq);

while True:
  try:
        data, addr = sock.recvfrom(port);
        print ("Received on " + str(group) + ":" + str(port));
  except KeyboardInterrupt:
        sys.exit(0);
