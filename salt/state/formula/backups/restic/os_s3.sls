# RESTIC backups to AWS S3
include:
    - formula.backups.restic

restic_creds:
    file.managed:
        - name: /etc/restic/cred_s3
        - source: salt://{{ slspath }}/files/cred_s3.j2
        - template: jinja
        - makedirs: True
        - user: root
        - group: root
        - mode: 0600
        - unless: test -f /etc/restic/cred_s3

restic_include:
    file.managed:
        - name: /etc/restic/include
        - source: salt://{{ slspath }}/files/include.j2
        - template: jinja
        - makedirs: True
        - user: root
        - group: root
        - mode: 0600

restic_exclude:
    file.managed:
        - name: /etc/restic/exclude
        - source: salt://{{ slspath }}/files/exclude.j2
        - template: jinja
        - makedirs: True
        - user: root
        - group: root
        - mode: 0600

run_S3_backup:
    file.managed:
        - name: /etc/restic/restic_os_s3.sh
        - source: salt://{{ slspath }}/files/restic_os_s3.sh
        - makedirs: True
        - user: root
        - group: root
        - mode: 0700
