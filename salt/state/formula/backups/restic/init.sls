## Restic Init

restic_dir:
    file.directory:
        - name: /etc/restic
        - makedirs: True
        - mode: "0600"
        - user: root
        - group: root

restic_bin:
    file.managed:
        - name: /usr/local/bin/restic
        - source: salt://restic # v 0.15  Salt repo
        - user: root
        - group: root
        - mode: "0700"

restic_symlink:
    file.symlink:
        - name: /usr/bin/restic
        - target: /usr/local/bin/restic