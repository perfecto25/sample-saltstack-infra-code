# RESTIC - host backups using a time snapshot

https://restic.readthedocs.io/en/stable/045_working_with_repos.html

company uses Restic to backup host data to Amazon S3 storage

## Installation & config

1. add Retic repo
2. yum install restic

add a Restic credential file to root (restic PW is kept in Bitwarden)

```
vim /root/.restic

export RESTIC_REPOSITORY=s3:https://s3.amazonaws.com/bucket-name/restic/<name of host>
export AWS_SECRET_ACCESS_KEY=<key>
export AWS_ACCESS_KEY_ID=<id>
export RESTIC_PASSWORD="<pw>"
```

initialize S3 Repo

```
source /etc/restic/cred
restic init

```

## Backing up directories

backup a single directory

    restic backup /home/user

create a file that contains paths to backup (include and exclude), run restic

    restic backup --files-from=restic_include.conf --exclude-file=restic_exclude.conf

    open repository
    repository 73974eeb opened successfully, password is correct

    Files:        4136 new,     0 changed,     0 unmodified
    Dirs:            3 new,     0 changed,     0 unmodified
    Added to the repo: 261.182 MiB

    processed 4136 files, 285.394 MiB in 0:18
    snapshot a40aec1c saved

## Check file difference

use diff to compare different snapshots
show snapshots

    restic snapshots

    repository 73974eeb opened successfully, password is correct
    ID        Time                 Host        Tags        Paths
    -------------------------------------------------------------------------
    a40aec1c  2019-02-27 16:08:54  awstestbox              /home  /root

    16d01d35  2019-02-27 16:13:19  awstestbox              /home  /root
    -------------------------------------------------------------------------
    2 snapshots

compare diff between 2 snapshots

    restic diff a40aec1c 16d01d35

    repository 73974eeb opened successfully, password is correct
    comparing snapshot a40aec1c to 16d01d35:

    +    /root/test.txt
    M    /var/log/messages
    M    /var/spool/mail/root

    Files:           1 new,     0 removed,     2 changed

## Find a file in a snapshot

    restic find /etc/hosts

## List files in latest snaphot

    restic ls -l latest

## Cat file in a snapshot

    restic dump latest /etc/hosts (or insert snapshot ID instead of latest)

## Restoring from a snapshot

example, restore a user's /home directory from latest snapshot

    restic restore latest --target / --include /home/<user> --cache-dir /mnt/hc/tmp

to restore to a specific dir (temporary dir)

    restic restore latest --target /tmpdir --include /home/<user> --cache-dir /mnt/hc/tmp

to restore all paths,

    restic restore latest (or snapshot ID) --target /

unlock repository (remove lock from previous procs)

    restic unlock -r REPO_NAME

## Cron

configure restic to run daily and email status

    0 0 * * * sh /etc/restic/run_backup.sh

## Removing old snapshots

remove a snapshot by ID

    restic forget SNAPSHOT_ID
    restic prune

keep only last 5 snapshots, remove all others

    restic forget --keep-daily 5
    restic prune

## Mount S3 as a file server

install fusermount

    yum install fuse
    restic mount /mnt/restic

restic will mount the S3 bucket. Quit the process to unmount.

all files are located under

    /mnt/restic/snapshots/latest

## Fix CA error

    cd /etc/pki/ca-trust/source/anchors
    openssl s_client -showcerts -servername s3.amazonaws.com -connect s3.amazonaws.com:443 > s3amazonaws.pem
    openssl x509 -inform PEM -in s3amazonaws.pem -text -out certdata
    update-ca-trust

or copy PEM file from FILES dir to /etc/pki/ca-trust/source/anchors

add to /etc/restic/cred

    export https_proxy=http://host1:3128
