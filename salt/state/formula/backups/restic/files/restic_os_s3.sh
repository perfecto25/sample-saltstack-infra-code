#!/bin/bash
# Runs Restic backup on a schedule via cron, emails with status

RDIR="/etc/restic"

### keep last # of snapshots
KEEP=5
JOBNAME="restic-os-S3"
CRED="/etc/restic/cred_s3"


cd $RDIR || exit

if [ ! -f $CRED ]
then
    /etc/salt/watcher.py $JOBNAME "cred file is not present" 1
	echo "${CRED} file not present, exiting..\n\n"
	exit 1
fi

source "${CRED}"
echo -e "$(date +%Y%m%d)"

msg=$(restic backup --files-from=include --exclude-file=exclude --no-cache)

if [ $? -eq 1 ]
then
    /etc/salt/watcher.py $JOBNAME "Error: running 'restic backup' ${msg}" 1
    exit 1
fi

msg=$(restic check)

# Check for Errors
if [ $? -eq 1 ]
then
    /etc/salt/watcher.py $JOBNAME "restic check failed: ${msg}" 1
    exit 1
fi


echo "removing old snapshots.."

msg=$(restic forget --keep-last ${KEEP} --prune --no-cache)

if [ $? -eq 1 ]
then
    /etc/salt/watcher.py $JOBNAME "restic forget failed: ${msg}" 1
    exit 1
fi


echo "end of run\n-----------------------------------------\n\n"

# notify OK
/etc/salt/watcher.py $JOBNAME "Restic OS backup to AWS S3 ok" 0
