manage_motd:
    file.managed:
        - name: /etc/motd
        - source: salt://{{ slspath }}/files/motd_default.j2
        - template: jinja 
        - user: root
        - group: root
        - mode: 644
