{% import_yaml tpldir ~ "/map.yaml" as data %}
{% set cfg = data["nodes"] %}


{% set target = pillar.get("target") %}

generate_nebula_cert_for_{{target}}:
    cmd.run:
        - name: nebula-cert sign -name "{{ target }}" -ip "{{ cfg[target]['neb_ip'] }}/22"
        - cwd: /opt/nebula/certs
        - unless: test -f /opt/nebula/certs/{{ target }}.crt
