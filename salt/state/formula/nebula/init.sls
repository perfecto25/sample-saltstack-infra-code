{% import_yaml tpldir ~ "/map.yaml" as data %}
{% set cfg = data["nodes"] %}

{% do salt.log.error(cfg) %}
{% set host = grains.id %}

nebula_dir:
    file.directory:
        - name: /opt/nebula/certs
        - makedirs: True
        - mode: "0600"
        - user: root
        - group: root

nebula_bin:
    file.managed:
        - name: /usr/local/bin/nebula
        - source: salt://nebula-1.6.1.centos7
        - user: root
        - group: root
        - mode: "0700"

nebula_cfg:
    file.managed:
        - name: /opt/nebula/config.yaml
        - source: salt://{{ slspath }}/files/config.yaml.j2
        - template: jinja
        - user: root
        - group: root
        - mode: "0644"
        - context:
            cfg: {{cfg}}

nebula_ca_cert:
    file.managed:
        - name: /opt/nebula/certs/ca.crt
        - source: salt://ca.crt
        - mode: "0600"
        - user: root
        - group: root

nebula_crt:
    file.managed:
        - name: /opt/nebula/certs/{{ host }}.crt
        - source: salt://{{ host }}.crt
        - mode: "0600"
        - user: root
        - group: root

nebula_key:
    file.managed:
        - name: /opt/nebula/certs/{{ host }}.key
        - source: salt://{{ host }}.key
        - mode: "0600"
        - user: root
        - group: root

nebula_systemd:
    file.managed:
        - name: /etc/systemd/system/nebula.service
        - source: salt://{{ slspath }}/files/nebula.service
        - user: root
        - group: root
        - mode: "0644"

nebula_service:
    service.running:
        - name: nebula
        - enable: True
        - watch:
            - file: nebula_cfg