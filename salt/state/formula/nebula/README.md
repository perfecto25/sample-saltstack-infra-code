# Slack Nebula


DOCS:  https://nebula.defined.net/docs/config/preferred-ranges/


Create CA, sign "lighthouse" server, plus nodes

    CA should be separate from lighthouses and nodes, can be offline server, only used to generate certs for nodes

    ./nebula-cert ca -name "nebulaCA" -duration 17531h (2 year cert span)

    # 1,024 possible ips, 255.255.252.0 mask
    ./nebula-cert sign -name "saltmaster" -ip "10.99.0.1/22"  # .0 = infra hosts
    ./nebula-cert sign -name "nycweb1" -ip "10.99.1.1/22"  # .1 = prod hosts
    ./nebula-cert sign -name "nycweb2" -ip "10.99.2.1/22"  # .2 = sim hosts
    ./nebula-cert sign -name "nycweb3" -ip "10.99.3.1/22"  # .3 = dev/uat/etc hosts


get nebula IP of each node

    nebula-cert print -path certs/nycweb1.crt
    NebulaCertificate {
	Details {
		Name: nycweb1
		Ips: [
			10.99.0.2/22
		]
		Subnets: []
		Groups: []
		Not before: 2022-11-29 23:13:34 -0500 EST
		Not After: 2023-11-29 23:13:14 -0500 EST
		Is CA: false
		Issuer: 17a7832a812148dw31507dbe1a21bc97731df871aa7bbc067277d62dcf7a06de9
		Public key: b49d54da3v4df09xve76a676221ecbdeb5df36e26fc01cab4df23ce571c33b293c
	}
	Fingerprint: a2689308d7eg05922602790e80cd153b39a2b91b8818e3ae475623b8161c0360
	Signature: 513a03bx23212219d98a82d8b8e033e134f530633e96564d91d6693cbc9bac163ed20226a1dcff96d1131499fc978686735919025ec1ab5e89ea721c1eacdf30b
    }


## Iptables

-A INPUT -s 10.99.0.0/22 -j ACCEPT
-A INPUT -p udp --dport 4242 -j ACCEPT

## Orchestrate Cert deployment

	 salt-run state.orchestrate nebula saltenv=dev pillar='{"target": "nycweb1"}'
