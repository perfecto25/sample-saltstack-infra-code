# STATE - Grafana Server

grafana_yum_repo:
    file.managed:
        - name: /etc/yum.repos.d/grafana.repo
        - source: salt://{{ slspath }}/files/grafana.repo
        - user: root
        - group: root
        - mode: 644

grafana_pkg:
    pkg.installed:
        - name: grafana

/etc/grafana/grafana.ini:
    file.managed:
        - source: salt://{{ slspath }}/files/grafana.ini.j2
        - template: jinja
        - user: grafana
        - group: grafana
        - require:
            - pkg: grafana_pkg

grafana_service:
    service.running:
        - name: grafana-server
        - enable: True
        - watch:
            - file: /etc/grafana/grafana.ini
