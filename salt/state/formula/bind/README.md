# DNS using BIND


on a client, edit /etc/resolv.conf

```
search company.nyc
nameserver <pub IP of DNS host 1>
nameserver <pub IP of DNS host 2>
```

## check syntaxnamed-checkzone localhost /var/named/zones/forward.company.nyc
```
named-checkzonenamed-checkzone localhost /var/named/zones/forward.company.nyc nyc 
named-checkconf /etc/named.conf 
```