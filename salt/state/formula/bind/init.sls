# BIND DNS Server + Resolver

{% set zones = salt['pillar.get']('dns:zones') %}

{% set pkgs = ['bind', 'bind-utils'] %}
{% for pkg in pkgs %}
{{ pkg }}_install:
    pkg.installed:
        - name: {{ pkg }}
{% endfor %}

named_config:
    file.managed:
        - name: /etc/named.conf
        - source: salt://{{ slspath }}/files/named.conf.j2
        - template: jinja
        - user: root
        - group: root
        - mode: 644
        - context:
            type: {{ salt['pillar.get']('dns:type', 'master') }}
            peer_ip: {{ salt['pillar.get']('dns:peer_ip', '127.0.0.1') }}
            zones: {{ salt['pillar.get']('dns:zones') }}
            listen: {{ salt['pillar.get']('dns:listen') }}

{% for zone in zones %}
{{ zone }}:
    file.managed:
        - name: /var/named/zones/forward.{{ zone }}
        - source: salt://{{ slspath }}/files/zones/forward.{{ zone }}.j2
        - template: jinja
        - makedirs: True
        - user: root
        - group: root
        - mode: 655
{% endfor %}

named_service:
    service.running:
        - name: named
        - enable: True
        - watch:
            - file: named_config
            {% for zone in zones %}
            - file: {{ zone }}
            {% endfor %}
