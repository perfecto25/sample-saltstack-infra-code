# STATE - IPTABLES
{% set iptables_file = '/etc/sysconfig/iptables' %}
{% if grains.osmajorrelease in [7,8] %}
{% set name = "iptables" %}
{% elif grains.osmajorrelease == 9 %}
{% set name = "iptables-nft" %}
{% endif %}


iptables_pkg:
    pkg.installed:
        - name: {{ name }}

{{ iptables_file }}:
    file.managed:
        - user: root
        - group: root
        - mode: "0644"
        - source:
            - salt://{{ slspath }}/files/env/{{ salt['pillar.get']('region') }}_{{ salt['pillar.get']('companyenv') }}.j2
            - salt://{{ slspath }}/files/env/{{ salt['pillar.get']('profile') }}_{{ salt['pillar.get']('companyenv') }}.j2
            - salt://{{ slspath }}/files/{{ grains.id }}.j2
            - salt://{{ slspath }}/files/default.j2
        - template: jinja
        - context:
            sls: {{ sls }}
            slspath: {{ slspath }}

check_syntax:
    cmd.run:
        - name: "iptables-restore --test {{ iptables_file }}"
        - onchanges:
            - file: "{{ iptables_file }}"

flush_tables:
    iptables.flush:
        - table: filter
        - family: ipv4
        - onchanges:
            - file: "{{ iptables_file }}"
            - cmd: check_syntax

restore_tables:
    cmd.run:
        - name: "iptables-restore < {{ iptables_file }}"
        - onchanges:
            - file: "{{ iptables_file }}"
            - cmd: check_syntax

{{ name }}-services:
    pkg.installed: []
    service.running:
        - name: iptables
        - enable: True
