# STATE - Network Routes

# RHEL7 scripts routes
{% if grains.osmajorrelease in [7,8] %}
{% if pillar.route_interfaces is defined %}
{% for interface in pillar.route_interfaces -%}
route-{{ interface }}:
    file.managed:
        - name: /etc/sysconfig/network-scripts/route-{{ interface }}
        - source:
            - salt://{{ slspath }}/files/routes/{{ grains.id }}.j2
        - template: jinja
        - user: root
        - group: root
        - mode: 644
        - context:
            interface: {{ interface }}
{% endfor %}
{% endif %}
{% endif %}


