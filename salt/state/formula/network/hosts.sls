# STATE - Default /etc/hosts management

{% set region = salt['pillar.get']('region')  %}
{{ region }}:
    file.managed:
        - name: /etc/hosts
        - source: 
            - salt://{{ slspath }}/files/hosts/{{ salt['pillar.get']('region') }}.j2
            - salt://{{ slspath }}/files/hosts/default.j2
        - template: jinja
        - user: root
        - group: root
        - mode: 644    
