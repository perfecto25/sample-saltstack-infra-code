# STATE - Network Interfaces


{% if pillar.network_interfaces is defined %}

## RHEL7 
{% if grains.osmajorrelease in [7, 8] %}
{% for iface in pillar.network_interfaces -%}
network-{{ iface }}:
    file.managed:
        - name: /etc/sysconfig/network-scripts/ifcfg-{{ iface }}
        - source: salt://{{ slspath }}/files/interfaces/{{ grains.id }}.j2
        - template: jinja
        - user: root
        - group: root
        - mode: 644
        - context:
            iface: {{ iface }}
{% endfor %}


## RHEL 9 NetworkManager
{% elif grains.osmajorrelease == 9 %}

{% import_yaml tpldir ~ "/files/routes/" + grains.id + ".yaml" as route_data %}

{% for iface in pillar.network_interfaces -%}
network-manager-{{ iface }}:
    file.managed:
        - name: /etc/NetworkManager/system-connections/con-{{ iface }}.nmconnection
        - source: salt://{{ slspath }}/files/interfaces/{{ grains.id }}.j2
        - template: jinja
        - user: root
        - group: root
        - mode: "0600"
        - context:
            iface: {{ iface }}
            route_data: {{ route_data }}
            tpldir: {{ tpldir }}
{% endfor %}

{% endif %}
{% endif %}
