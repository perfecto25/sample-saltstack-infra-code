{% if salt['pillar.get']('cron:present') %}
{% for cron in salt['pillar.get']('cron:present') -%}
{% set val = salt['pillar.get']('cron:present:{}'.format(cron)) %}

set_cron_{{ cron }}:
    cron.present:
        - name: "{{ val.cmd }}"
        - identifier: "{{ cron }}"
        {% if 'special' in val %}
        - special: "{{ val.special }}"
        {% else %}
        - minute: "{{ val.minute|default('*') }}"
        - hour: "{{ val.hour|default('*') }}"
        - daymonth: "{{ val.daymonth|default('*') }}"
        - month: "{{ val.month|default('*') }}"
        - dayweek: "{{ val.dayweek|default('*') }}"
        {% endif %}

    {% if 'user' in val %}
        - user: {{ val.user }}
    {% endif %}

    {% if 'comment' in val %}
        - comment: {{ val.comment }}
    {% endif %}
{% endfor %}
{% endif %}


{% if salt['pillar.get']('cron:absent') %}
{% for cron in salt['pillar.get']('cron:absent') -%}
{% set val = salt['pillar.get']('cron:absent:{}'.format(cron)) %}

unset_cron_{{ cron }}:
    cron.absent:
        - identifier: "{{ cron }}"
{% endfor %}
{% endif %}
