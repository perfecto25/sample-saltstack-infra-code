# Cron config

allow non-root users to manage their own crons

    vim /etc/cron.allow
    add username to this file

    remove /etc/cron.deny

    chown root:companyusers /usr/bin/crontab
    chmod 4755 /usr/bin/crontab