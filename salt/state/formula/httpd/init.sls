# STATE - APACHE

install_apache:
    pkg.installed:
        - name: httpd

{% set mods = ['mod_ssl'] %}

{% for mod in mods %}
install_mod_{{ mod }}:
    pkg.installed:
        - name: {{ mod }}
{% endfor %}

httpd_conf:
    file.managed:
        - name: /etc/httpd/conf/httpd.conf
        - source: salt://{{ slspath }}/files/httpd.conf.j2
        - template: jinja
        - user: root
        - group: root
        - mode: 644

{% if salt['pillar.get']('httpd:vhosts') %}
{% for conf in salt['pillar.get']('httpd:vhosts') %}
{{ conf }}_file:
    file.managed:
        - name: /etc/httpd/conf.d/{{ conf }}.conf
        - source: salt://{{ slspath }}/files/configs/{{ conf }}.j2
        - template: jinja
        - user: root
        - group: root
        - mode: 644
{% endfor %}
{% endif %}

selinux_network_connect:
    selinux.boolean:
        - name: httpd_can_network_connect
        - value: on
        - persist: True

run_service:
    service.running:
        - name: httpd
        - watch: 
            - file: /etc/httpd/conf/httpd.conf
            {% if salt['pillar.get']('httpd:vhosts') %}
            {% for conf in salt['pillar.get']('httpd:vhosts') %}
            - file: /etc/httpd/conf.d/{{ conf }}.conf
            {% endfor %}
            {% endif %}

# ## certbot auto renew
certbot_renew:
    file.managed:
        - name: /etc/httpd/certbot.sh
        - source: salt://{{ slspath }}/files/certbot.sh
        - user: root
        - group: root
        - mode: 0744

/etc/httpd/certbot.sh:
    cron.present:
        - identifier: cerbot auto renew
        - user: root
        - daymonth: "*/45"
        - hour: 5
        - minute: 0     