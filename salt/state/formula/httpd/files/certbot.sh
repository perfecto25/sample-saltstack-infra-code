#!/bin/bash

/sbin/iptables -I INPUT 1 -p tcp -m multiport --dports 80,443 -j ACCEPT

/bin/certbot renew --apache

# Check for Errors
if [ $? -eq 1 ]
then
    /bin/mail -s "certbot auto renew failed on $(hostname)" admin@company.com < /dev/null
    /sbin/iptables-restore < /etc/sysconfig/iptables
    exit 1
fi

/sbin/iptables -D INPUT 1
