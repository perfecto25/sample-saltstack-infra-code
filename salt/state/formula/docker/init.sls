# STATE - installs Docker CE and Compose


docker-ce-repo:
    cmd.run:
        - name: yum-config-manager --add-repo https://download.docker.com/linux/centos/docker-ce.repo
        - unless: test -f /etc/yum.repos.d/docker-ce.repo

{% for pkg in 
    'yum-utils', 
    'device-mapper-persistent-data', 
    'lvm2',
    'docker-ce',
    'docker-ce-cli',
    'docker-compose-plugin' %}
{{ pkg }}:
    pkg.installed: []
{% endfor %}


docker-service:
    service.running:
        - name: docker
        - enable: True
