## Crontab lines
## pass crontab pillar in form of
## crontab:
##   <user>:
##      <cron description>: <cron command>
## ie:
## crontab:
##     root:
##         database backup: "*/30 * * * MON-FRI /bin/pgbackup > /tmp/file"

{% if pillar.get('crontab') -%}

{% for user, crons in pillar.get('crontab', {}).items() %}

{#% do salt.log.error(crons) %#}

{% for comment, cron in crons.items() %}
{% set cronline = cron.split(' ') %}
{% set cmd = cronline[5:]|join(' ')%}

"{{ user }}_{{ cmd }}":
    cron.present:
        - name: "{{ cmd }}"
        - user: "{{ user }}"
        - minute: "{{ cronline[0] }}"
        - hour: "{{ cronline[1] }}"
        - daymonth: "{{ cronline[2] }}"
        - month: "{{ cronline[3] }}"
        - dayweek: "{{ cronline[4] }}"
        - identifier: "{{ comment }}"
{% endfor %}

{% endfor %}
{% endif %}