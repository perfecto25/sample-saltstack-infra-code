# STATE:  HA PROXY

haproxy:
    pkg:
        - installed
    service.running:
        - watch:
            - pkg: haproxy
            - file: /etc/haproxy/haproxy.cfg
            - file: /etc/default/haproxy


/etc/haproxy/haproxy.cfg:
    file.managed:
        - source: salt://{{ slspath }}/files/{{ grains.id }}.cfg.j2
        - template: jinja
        - user: root
        - group: root
        - mode: 644

/etc/default/haproxy:
    file.managed:
        - source: salt://{{ slspath }}/files/default
        - user: root
        - group: root
        - mode: 644