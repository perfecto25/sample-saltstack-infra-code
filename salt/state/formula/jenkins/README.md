# Jenkins

## install

download Jenkins WAR file, place in /opt/jenkins

restart jenkins service

    systemctl restart jenkins

all Jenkins build, plugin, user data is in /home/jenkins/jenkins (backed up by Restic to S3)


## upgrade

stop jenkins service

download latest WAR file from jenkins site

restart jenkins service




## Installing Plugins with CLI tool

get Plugin CLI from jenkins

     wget http://localhost:8080/jnlpJars/jenkins-cli.jar -O /var/lib/jenkins/jenkins-cli.jar

install plugin

    root@jenkins:opt $ java -jar /var/lib/jenkins/jenkins-cli.jar -s "http://localhost:8080" -auth user:password -noKeyAuth install-plugin blackduck-detect:3.1.0 -restart

