install_java:
    pkg.installed:
        - pkgs:
            - java-11-openjdk-devel
            - java-11-openjdk

jenkins_group:
    group.present:
        - name: jenkins
        - gid: 2007

jenkins_user:
    user.present:
        - name: jenkins
        - uid: 2007
        - createhome: True
        - allow_uid_change: True
        - groups:
            - jenkins

jenkins_log:
    file.directory:
        - name: /var/log/jenkins
        - user: jenkins
        - group: jenkins
        - dir_mode: "0750"

jenkins_systemd:
    file.managed:
        - name: /usr/lib/systemd/system/jenkins.service
        - source: salt://{{ slspath }}/files/jenkins.systemd
        - mode: "0644"
        - user: root
        - group: root

jenkins_service:
   service.running:
       - name: jenkins

Development Tools:
    pkg.group_installed: []