install_node:
    cmd.run: 
        - name: curl -sL https://rpm.nodesource.com/setup_10.x | sudo -E bash -
        - unless: rpm -qa | grep nodejs

    pkg.installed:
        - name: nodejs
    
    npm.installed:
        - pkgs: ['http-server']
