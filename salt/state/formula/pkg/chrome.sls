

get_binary:
    cmd.run:
        - name: cd /tmp && wget https://dl.google.com/linux/direct/google-chrome-stable_current_x86_64.rpm
        - unless: rpm -qa | grep google-chrome-stable
install:
    cmd.run:
        - name: yum -y localinstall /tmp/google-chrome-stable_current_x86_64.rpm
        - unless: rpm -qa | grep google-chrome-stable
clean:
    cmd.run:
        - name: rm -f /tmp/google-chrome-stable_current_x86_64.rpm
        - unless: rpm -qa | grep google-chrome-stable