# R language install - Rocky 8,9

{% set version = '4.2.1' %}
{% if grains.osmajorrelease == 8 %}
{% set path = "centos-8" %}
{% elif grains.osmajorrelease == 9 %}
{% set path = "redhat-9" %}
{% endif %}

Development Tools:
    pkg.group_installed: []

install_R_prereqs:
    pkg.installed:
        - pkgs:
            - gcc-gfortran
            - libcurl
            - libcurl-devel
            - epel-release
            - libxml2-devel
            - openssl-devel
            - texlive-latex
            - texlive-mathtools
            - texlive-xetex
            - readline-devel
            - cairo-devel
            - libXt-devel
            - fribidi-devel
            - harfbuzz-devel
            - freetype-devel
            - libpng-devel
            - libtiff-devel
            - libjpeg-turbo-devel
        - failhard: True

install_R_rpm:
    pkg.installed:
        - sources:
            - R-{{version}}: https://cdn.rstudio.com/r/{{ path }}/pkgs/R-{{ version }}-1-1.x86_64.rpm
        - failhard: True

create_R_symlink:
    file.symlink:
        - name: /usr/bin/R
        - target: /opt/R/{{version}}/bin/R
        - force: True
        - mode: "0444"
        - user: root
        - group: root

tmp_exec:
    cmd.run:
        - name: mount -o remount,exec /tmp

{% set r_pkgs = [
    'stringr',
    'timeDate',
    'grDevices',
    'methods',
    'limSolve',
    'quadprog',
    'Cairo'
] %}

{% for pkg in r_pkgs %}
{{ pkg }}_R_install:
    cmd.run:
        - name: echo "install.packages(\"{{ pkg }}\", dependencies = TRUE, repos=\"https://cran.rstudio.com\")" | R --no-save
{% endfor %}

remove_old_R_packages:
    cmd.run:
        - name: echo "if('qserver' %in% rownames(installed.packages())) remove.packages('qserver')" | R --no-save

install_r_curl:
    cmd.run:
        - name: echo "install.packages(\"https://cran.r-project.org/src/contrib/Archive/curl/curl_4.3.tar.gz\",repo=NULL,type=\"source\")" | R --no-save

install_devtools:
    cmd.run:
        - name: echo "install.packages('devtools', repos=\"https://cran.rstudio.com\")" | R --no-save

install_rkdb:
    cmd.run:
        - name: echo "devtools::install_github('kxsystems/rkdb', quiet=TRUE)" | R --no-save

remount_tmp_noexec:
    cmd.run:
        - name: mount -o remount,noexec /tmp
