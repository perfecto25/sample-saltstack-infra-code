mongo_repo:
    file.managed:
        - name: /etc/yum.repos.d/mongo.repo
        - source: salt://{{ slspath }}/repo
        - user: root
        - group: root
        - mode: 644

mongo_pkg:
    pkg.installed:
        - name: mongodb-org

mongo_service:
    service.running:
        - name: mongod.service   
        - enable: True