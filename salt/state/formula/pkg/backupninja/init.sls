# STATE - Backup Ninja

ninja_pkg:
    pkg.installed:
        - name: backupninja

ninja_config:
    file.managed:
        - name: /etc/backupninja.conf
        - source: salt://{{ slspath }}/files/backupninja.conf
        - user: root
        - group: root
        - mode: 644
        - require:
            - pkg: ninja_pkg 

rdiff_config:
    file.managed:
        - name: /etc/backup.d/{{ grains.id }}.rdiff
        - source: salt://{{ slspath }}/files/source.rdiff.j2
        - template: jinja
        - user: root
        - group: root
        - mode: 600
        - require:
            - pkg: ninja_pkg
