# Backup Ninja

runs nightly backups of various folders and files

## Config

1. install backupninja on host where backups will be synced to (backup Master)
1. install backupninja on host that will be syncing its own files to the Master (backups source)
1. on Master, create a backupninja user acct (see UID + GID from wiki)
1. on Source, create a SSH private key for 'root' account and copy this public key to the the Master's 'backupninja' user acct's Authorized Keys file
1. test SSHing from Source to Master
    ```
    source> ssh backupninja@master
    ```
1. on Master, create a location where backups will be stored, ie /mnt/hc/server_backups/backupninja
1. chown backupninja:backupninja /data/server_backups/backupninja

## Cmd Line options
```
backupninja -t  // run Test
backupninja -n  // run now
backupninja -d  // debug
```