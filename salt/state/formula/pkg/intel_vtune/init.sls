intel_vtune_repo:
    file.managed:
        - name: /etc/yum.repos.d/oneAPI.repo
        - source: salt://{{ slspath }}/repo
        - user: root
        - group: root
        - mode: 644

intel-oneapi-vtune:
    pkg.installed: []
