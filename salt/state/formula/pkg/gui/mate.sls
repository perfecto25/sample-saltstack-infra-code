# State - MATE graphical user interface

epel_pkg:
    pkg.installed:
        - name: epel-release

X Window system:
    pkg.group_installed: []

install_gui:
    cmd.run:
        - name: "yum -y groupinstall 'MATE Desktop' 
        --exclude=abrt* 
        --exclude=rhythmbox 
        --exclude=lohit* 
        --exclude=simple-scan 
        --exclude=brasero 
        --exclude=transmission-gtk 
        --exclude=xchat"

isolate:
    cmd.run:
        - name: systemctl isolate graphical.target

default_GUI:
    cmd.run: 
        - name: systemctl set-default graphical.target

gnome-disk-utility:
    pkg.installed: []
