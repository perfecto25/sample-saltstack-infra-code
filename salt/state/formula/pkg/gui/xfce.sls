# State - MATE graphical user interface

epel_pkg:
    pkg.installed:
        - name: epel-release

X Window system:
    pkg.group_installed: []

install_gui:
    cmd.run:
        - name: "yum -y groupinstall xfce 
        --exclude=abrt* 
        --exclude=rhythmbox 
        --exclude=lohit* 
        --exclude=simple-scan 
        --exclude=brasero 
        --exclude=transmission-gtk 
        --exclude=xchat
        --exclude=empathy"


idle_delay:
   cmd.run:
       - name: "gsettings set org.gnome.desktop.session idle-delay 0"

gdm:
    service.running:
        - name: gdm
        - enable: True

default_GUI:
    cmd.run: 
        - name: "systemctl isolate graphical.target && systemctl set-default graphical.target"

