# State - GNOME graphical user interface

epel_pkg:
    pkg.installed:
        - name: epel-release

X Window system:
    pkg.group_installed: []

install_gui:
    cmd.run:
        - name: "yum -y groupinstall 'GNOME Desktop' 
        --exclude=abrt* --exclude=empathy"

disable_screen_lock:
    cmd.run:
        - name: gconftool-2 --set /schemas/desktop/gnome/lockdown/disable_lock_screen --type boolean true

disable_screen_lock_dconf:
    cmd.run:
        - name: gsettings set org.gnome.desktop.session idle-delay 0

gdm:
    service.running:
        - enable: True
        - reload: True


isolate:
    cmd.run:
        - name: systemctl isolate graphical.target

default_GUI:
    cmd.run: 
        - name: systemctl set-default graphical.target

gnome-disk-utility:
    pkg.installed: []
