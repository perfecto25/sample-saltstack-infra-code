install_git2_rpm:
    cmd.run:
        - name: rpm -U http://opensource.wandisco.com/centos/7/git/x86_64/wandisco-git-release-7-2.noarch.rpm
        - unless: rpm -qa | grep git-2
    pkg.installed:
        - name: git
