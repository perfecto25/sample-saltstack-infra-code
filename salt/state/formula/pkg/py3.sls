{% set version = '3.11.1' %}
{% set majver = '3.11' %}

install_py3_required_packages:
    pkg.installed:
        - pkgs: ['libffi-devel', 'gcc', 'openssl-devel', 'bzip2-devel', 'wget', 'make', 'sqlite-devel', 'zlib-devel']
        - failhard: True

## add SQLite module
sqlite_tar:
  archive.extracted:
    - name: /opt/sqlite_tmp
    - source: https://sqlite.org/2022/sqlite-autoconf-3390200.tar.gz
    - skip_verify: True
    - failhard: True

sqlite_make:
    cmd.run:
        - name: ./configure && make && make install
        - cwd: /opt/sqlite_tmp/sqlite-autoconf-*
        - unless: test -f /usr/local/bin/sqlite3
        - failhard: True

# unarchive in /opt since /tmp is disabled for executables (CIS compliance)
python_tar:
  archive.extracted:
    - name: /opt/python_tmp
    - source: https://www.python.org/ftp/python/{{ version }}/Python-{{ version }}.tgz
    - skip_verify: True
    - failhard: True

configure_and_make:
    cmd.run:
        - name: ./configure --with-system-ffi --with-computed-gotos --enable-loadable-sqlite-extension && make -j ${nproc} && make altinstall
        - cwd: /opt/python_tmp/Python-{{ version }}
        - unless: test -f /usr/bin/python{{ majver }}
        - failhard: True

/usr/bin/python{{ majver }}:
    file.symlink:
        - target: /usr/local/bin/python{{ majver }}
        - failhard: True

remove_python_tmp:
    cmd.run:
        - name: rm -rf /opt/python_tmp
        - onlyif: test -d /opt/python_tmp

remove_sqlite_tmp:
    cmd.run:
        - name: rm -rf /opt/sqlite_tmp
        - onlyif: test -d /opt/sqlite_tmp

## to uninstall
# rm -rf /usr/local/bin/pip3.10
# rm -rf /usr/local/lib/libpython3.11.a
# rm -rf /usr/local/lib/python3.11
# rm -rf /usr/local/bin/python3.11*
