
{% set kernel = '4.18.0-372.32.1.el8_6' %}

kexec_pkg:
    pkg.installed:
        - name: kexec-tools

update_kernel:
    cmd.run:
        - name: 'yum -y update kernel\*'

clear_kexec:
    cmd.run:
        - name: "kexec -u"

load_new_kernel:
    cmd.run:
        - name: "kexec -l /boot/vmlinuz-{{ kernel }} --initrd=/boot/initramfs-{{ kernel }}.img --reuse-cmdline"
        - onlyif:
            - test -f /boot/vmlinuz-{{ kernel }}
            - test -f /boot/initramfs-{{ kernel }}.img

boot_new_kernel:
    cmd.run:
        - name: systemctl kexec
        - onlyif:
            - test -f /boot/vmlinuz-{{ kernel }}
            - test -f /boot/initramfs-{{ kernel }}.img