import_key:
    cmd.run:
        - name: rpm --import https://packages.microsoft.com/keys/microsoft.asc
        - unless: test -f /etc/yum.repos.d/vscode.repo

repo:
    file.managed:
        - name: /etc/yum.repos.d/vscode.repo
        - source: salt://{{ slspath }}/files/repo
        - user: root
        - group: root
        - mode: 644
    
code:
    pkg.installed: []