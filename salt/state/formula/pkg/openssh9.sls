install_openssh_required_packages:
    pkg.installed:
        - pkgs: ['zlib-devel', 'openssl-devel', 'bzip2-devel', 'wget', 'make', 'zlib-devel', 'pam-devel', 'libselinux-devel']
        - failhard: True


openssh_tar:
    archive.extracted:
        - name: /opt/openssh_tmp
        - source: https://cdn.openbsd.org/pub/OpenBSD/OpenSSH/portable/openssh-9.2p1.tar.gz
        - skip_verify: True
        - failhard: True

openssh_make:
    cmd.run:
        - name: ./configure  --with-pam --with-selinux --with-privsep-path=/var/lib/sshd/ --sysconfdir=/etc/ssh && make && make install
        - cwd: /opt/openssh_tmp/openssh-9.2p1
        #- unless:
        - failhard: True