chronyd_pkg:
    pkg.installed:
        - name: chrony

chronyd_conf:
    file.managed:
        - name: /etc/chrony.conf
        - source: salt://{{ slspath }}/files/chrony.conf
        - user: root
        - group: root
        - mode: 644

chronyd_service:
    service.running:
        - name: chronyd
        - enable: True