## AIRFLOW PROD

airflowprod_user:
    user.present:
        - name: airflow
        - fullname: airflow
        - uid: 2009
        - gid: 8102
        - shell: /bin/bash
        - home: /opt/airflow
        - createhome: True

airflowprod_home:
    file.directory:
        - name: /opt/airflow/prod
        - dir_mode: 0755
        - user: airflow
        - group: developers

## manually install virtualenv
## cd /opt/airflow/
## python3 -m venv venv
## source venv/bin/activate
## python3 -m pip install install apache-airflow[postgres,celery,ssh,github_enterprise]
## ln -s /opt/airflow/venv/bin/airflow /usr/bin/airflow

airflowprod_config:
    file.managed:
        - name: /opt/airflow/prod/airflow.cfg
        - source: salt://{{ slspath }}/files/airflow.cfg.j2
        - template: jinja
        - context: { env: prod, web_url: "airflow1", web_port: 8090, flower_port: 5555, worker_log_server_port: 8793 }
        - user: airflow
        - group: developers
        - mode: "0644"

airflowprod_env:
    file.managed:
        - name: /opt/airflow/prod/.env
        - source: salt://{{ slspath }}/files/env.j2
        - template: jinja
        - context: { env: prod, color: FFEF00 }
        - user: airflow
        - group: developers
        - mode: "0644"

airflowprod_webserver:
    file.managed:
        - name: /usr/lib/systemd/system/airflowprod-webserver.service
        - source: salt://{{ slspath }}/files/airflow-webserver.service.j2
        - template: jinja
        - context: { env: prod, pgver: 14 }
        - user: root
        - group: root
        - mode: "0644"

airflowprod_scheduler:
    file.managed:
        - name: /etc/systemd/system/airflowprod-scheduler.service
        - source: salt://{{ slspath }}/files/airflow-scheduler.service.j2
        - template: jinja
        - context: { env: prod, pgver: 14 }
        - user: root
        - group: root
        - mode: "0644"

airflowprod_worker:
    file.managed:
        - name: /etc/systemd/system/airflowprod-worker.service
        - source: salt://{{ slspath }}/files/airflow-worker.service.j2
        - template: jinja
        - context: { env: prod, pgver: 14 }
        - user: root
        - group: root
        - mode: "0644"

combined_prod_service:
    file.managed:
        - name: /opt/airflow/prod/service
        - source: salt://{{ slspath }}/files/service.j2
        - template: jinja
        - context: { env: prod }
        - user: airflow
        - group: developers
        - mode: "0755"

airflow_sudoers:
    file.managed:
        - name: /etc/sudoers.d/airflow
        - source: salt://{{ slspath }}/files/sudo-af
        - user: root
        - group: root
        - mode: "0644"

airflowprod_webserver_svc:
    service.running:
        - name: airflowprod-webserver
        - enable: True
        - watch:
            - file: airflowprod_config

airflowprod_scheduler_svc:
    service.running:
        - name: airflowprod-scheduler
        - enable: True
        - watch:
            - file: airflowprod_config

{# airflowprod_worker_svc:
    service.running:
        - name: airflowprod-worker
        - enable: True
        - watch:
            - file: airflowprod_config #}

pg_hba:
    file.managed:
        - name: /var/lib/pgsql/14/data/pg_hba.conf
        - source: salt://{{ slspath }}/files/pg_hba.conf
        - user: postgres
        - group: postgres
        - mode: "0600"

pg_service:
    service.running:
        - name: postgresql-14
        - enable: True
        - watch:
            - file: pg_hba
