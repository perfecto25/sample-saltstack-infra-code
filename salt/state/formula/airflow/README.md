# Apache AIRFLOW




### SIM environment

create new SIM environment (separate from Prod)

    mkdir /opt/airflow
    cd /opt/airflow/
    python3 -m venv venv
    source venv/bin/activate
    pip install apache-airflow['postgres']

create DB user

    # if user is not present (shoudl be present due to Prod environment)
    postgres=# CREATE USER airflow PASSWORD 'airflow';
    CREATE ROLE

    postgres=# CREATE DATABASE airflowsim;
    CREATE DATABASE
    postgres=# grant all privileges on database airflowsim to airflow;
    GRANT

Initialize DB (leave virtualenv if active)

    (venv) /opt/airflow
    exit
    source /opt/airflow/sim/.env

    # now init virtualenv
    cd /opt/airflow
    /opt/airflow > source venv/bin/activate
    (venv) /opt/airflow> airflow db init


Create Fernet key (update airflow.cfg with Fernet key)

    pip install cryptography
    python -c "from cryptography.fernet import Fernet; print(Fernet.generate_key().decode())"

Update Flask secret key in airflow.cfg (generate new key)

    openssl rand -hex 30
    paste into config file, secret_key

Create Admin user

    source .env
    airflow users create -f company -l company -p company1234 -r Admin -u company -e infraalerts@company.com


Start / Stop / Status of all AF services

    airflow@companyaf-us1:prod $ ./service stop (start/stop/status)




Change Background color and other settings via ENV variable

https://airflow.apache.org/docs/apache-airflow/stable/configurations-ref.html#navbar-color

## File backups

backup of /opt/airflow is done by Restic

/etc/restic/include


## Postgres Backups

cron backup

    root@companyreport-us1:opt $ runuser -l postgres  -c 'pg_dump -O -F c -f /opt/airflow/prod/backups/backup.dat -Z 3 --blobs -p 5432 -h localhost -d airflowprod'