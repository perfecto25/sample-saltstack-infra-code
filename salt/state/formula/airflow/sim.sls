## AIRFLOW SIM

airflow_user:
    user.present:
        - name: airflow
        - fullname: airflow
        - uid: "2009"
        - gid: "8102"
        - shell: /bin/bash
        - home: /opt/airflow
        - createhome: True
        - allow_uid_change: True
        - allow_gid_change: True

airflowsim_home:
    file.directory:
        - name: /opt/airflow/sim
        - dir_mode: "0755"
        - user: airflow
        - group: developers

## manually install virtualenv
## cd /opt/airflow/
## python3 -m venv venv
## source venv/bin/activate
## python3 -m pip install install apache-airflow[postgres,celery,ssh,github_enterprise]
## ln -s /opt/airflow/venv/bin/airflow /usr/bin/airflow

airflowsim_config:
    file.managed:
        - name: /opt/airflow/sim/airflow.cfg
        - source: salt://{{ slspath }}/files/airflow.cfg.j2
        - template: jinja
        - context: { env: sim, web_url: "companyreport-us1", web_port: 8095, flower_port: 5555, worker_log_server_port: 8793 }
        - user: airflow
        - group: developers
        - mode: "0644"

airflowsim_env:
    file.managed:
        - name: /opt/airflow/sim/.env
        - source: salt://{{ slspath }}/files/env.j2
        - template: jinja
        - context: { env: sim, color: 32a8a2 }
        - user: airflow
        - group: developers
        - mode: "0644"

airflowsim_webserver:
    file.managed:
        - name: /usr/lib/systemd/system/airflowsim-webserver.service
        - source: salt://{{ slspath }}/files/airflow-webserver.service.j2
        - template: jinja
        - context: { env: sim, pgver: 10 }
        - user: root
        - group: root
        - mode: "0644"

airflowsim_scheduler:
    file.managed:
        - name: /etc/systemd/system/airflowsim-scheduler.service
        - source: salt://{{ slspath }}/files/airflow-scheduler.service.j2
        - template: jinja
        - context: { env: sim, pgver: 10 }
        - user: root
        - group: root
        - mode: "0644"

airflowsim_worker:
    file.managed:
        - name: /etc/systemd/system/airflowsim-worker.service
        - source: salt://{{ slspath }}/files/airflow-worker.service.j2
        - template: jinja
        - context: { env: sim, pgver: 10 }
        - user: root
        - group: root
        - mode: "0644"

combined_sim_service:
    file.managed:
        - name: /opt/airflow/sim/service
        - source: salt://{{ slspath }}/files/service.j2
        - template: jinja
        - context: { env: sim }
        - user: airflow
        - group: developers
        - mode: "0755"

airflow_sudoers:
    file.managed:
        - name: /etc/sudoers.d/airflow
        - source: salt://{{ slspath }}/files/sudo-af
        - user: root
        - group: root
        - mode: "0644"

airflowsim_webserver_svc:
    service.running:
        - name: airflowsim-webserver
        - enable: True
        - watch:
            - file: airflowsim_config

airflowsim_scheduler_svc:
    service.running:
        - name: airflowsim-scheduler
        - enable: True
        - watch:
            - file: airflowsim_config

pg_hba:
    file.managed:
        - name: /var/lib/pgsql/10/data/pg_hba.conf
        - source: salt://{{ slspath }}/files/pg_hba.conf
        - user: postgres
        - group: postgres
        - mode: "0600"

pg_service:
    service.running:
        - name: postgresql-10
        - enable: True
        - watch:
            - file: pg_hba
