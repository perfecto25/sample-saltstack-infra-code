# AWS EC2 config
# Bootstraps Salt Minion agent on a host

set_hostname:
    cmd.run:
        - name: /bin/hostnamectl set-hostname --static {{ grains.id }}
    file.append:
        - name: /etc/cloud/cloud.cfg
        - text:
            - 'preserve_hostname: true'

/etc/hosts:
    file.replace:
        - repl: "127.0.0.1   localhost localhost.localdomain localhost4 localhost4.localdomain4 {{ grains.id }}"
        - pattern: "^127.0.0.1.*"

augeas:
    pkg.installed: []

include:
    - formula.saltstack.minion
    - formula.pkg.py3
    - formula.cis

add_master_ip:
    file.append:
        - name: /etc/hosts
        - text:
            - "<Your saltmaster IP>  saltmaster"

update_all_pkgs:
    cmd.run:
        - name: yum -y update

restart:
    cmd.run:
        - name: reboot

