# STATE - Minion Config

minion_config:
    file.managed:
        - name: /etc/salt/minion
        - source: salt://{{ slspath }}/files/minion.j2
        - template: jinja
        - mode: 644
        - user: root
        - group: root 

minion_systemd:
    file.managed:
        - name: /usr/lib/systemd/system/salt-minion.service
        - source: salt://{{slspath}}/files/minion.systemd
        - mode: 644
        - user: root
        - group: root

salt_minion_start:
    service.running:
        - name: salt-minion
        - enable: True
        - watch:
            - file: minion_config

# watcher script - creates timestamp of events (backups, AV scans, etc)
job_watcher:
    file.managed:
        - name: /etc/salt/watcher.py
        - source: salt://{{ slspath }}/files/watcher.py
        - template: jinja
        - mode: 744
        - user: root
        - group: root 
