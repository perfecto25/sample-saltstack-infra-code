
# SALTSTACK MASTER

master_conf:
    file.managed:
        - name: /etc/salt/master
        - source: salt://{{ slspath }}/files/master.yaml
        - user: root
        - group: root
        - mode: "0644"

master_conf_include:
    file.recurse:
        - name: /etc/salt/master.d
        - source: salt://{{ slspath }}/files/master.d
        - makedirs: True
        - user: root
        - group: root
        - file_mode: "0600"



master_service:
    service.running:
        - name: salt-master

include:
    - formula.saltstack.minion

