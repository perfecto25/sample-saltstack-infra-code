## EC2 integration

cloud_provider_ec2:
    file.managed:
        - name: /etc/salt/cloud.providers.d/ec2.conf
        - source: salt://{{ slspath }}/files/cloud.providers.d/ec2.conf.j2
        - template: jinja
        - user: root
        - group: root
        - mode: "0600"

cloud_profiles:
    file.recurse:
        - name: /etc/salt/cloud.profiles.d
        - source: salt://{{ slspath }}/files/cloud.profiles.d
        - makedirs: True
        - user: root
        - group: root
        - file_mode: "0600"