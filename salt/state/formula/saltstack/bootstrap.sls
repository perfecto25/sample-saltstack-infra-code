# bootstrap install salt minion


py3-devel:
    pkg.installed:
        - name: python3-devel

install_minion:
    cmd.script:
        - name: salt://{{ slspath }}/files/bootstrap.sh
        - cwd: /opt

minion_systemd:
    file.managed:
        - name: /usr/lib/systemd/system/salt-minion.service
        - source: salt://{{ slspath }}/files/minion.systemd
        - mode: "0644"
        - user: root
        - group: root

minion_config:
    file.managed:
        - name: /etc/salt/minion
        - source: salt://{{ slspath }}/files/minion.j2
        - template: jinja
        - mode: "0644"
        - makedirs: True
        - user: root
        - group: root

salt_minion_start:
    service.running:
        - name: salt-minion
        - enable: True
        - watch:
            - file: minion_config

# watcher script - creates timestamp of events (backups, AV scans, etc)
watcher:
    file.managed:
        - name: /etc/salt/watcher.py
        - source: salt://{{ slspath }}/files/watcher.py
        - template: jinja
        - mode: "0744"
        - user: root
        - group: root

include:
    - formula.saltstack.ec2