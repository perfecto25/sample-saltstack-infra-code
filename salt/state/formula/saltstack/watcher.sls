# WATCHER

job_watcher:
    file.managed:
        - name: /etc/salt/watcher.py
        - source: salt://{{ slspath }}/files/watcher.py
        - mode: 744
        - user: root
        - group: root