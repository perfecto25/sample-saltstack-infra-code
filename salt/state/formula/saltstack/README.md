# Configures SaltStack environment

## Configures Salt Master layout & settings

### Install Master

Salt Master and Minion are installed using Pip and Virtual Env (to avoid conflicts with global py packages)


      cd /opt
      python3.6 -m venv salt
      source salt/bin/activate
      pip install salt dictor pyinotify

if getting network error, run via proxy

      pip install --proxy <proxyIP>:<proxy Port> <pkg names>

all executables are in /opt/salt/bin


add symlinks for all services

      ln -s /opt/salt/bin/salt /usr/bin/salt
      ln -s /opt/salt/bin/salt-minion /usr/bin/salt-minion
      ln -s /opt/salt/bin/salt-ssh /usr/bin/salt-ssh
      ln -s /opt/salt/bin/salt-key /usr/bin/salt-key
      ln -s /opt/salt/bin/salt-call /usr/bin/salt-call
      ln -s /opt/salt/bin/salt-run /usr/bin/salt-run


add systemd scripts

add /etc/salt/master config file

      systemctl daemon-reload
      systemctl start salt-master


---
### Install Minion

      cd /opt
      python3.6 -m venv salt
      source salt/bin/activate
      pip install salt pyinotify
      ln -s /opt/salt/bin/salt-minion /usr/bin/salt-minion
      ln -s /opt/salt/bin/salt-call /usr/bin/salt-call


---
### Upgrade Master and Minion

      cd /opt/
      source salt/bin/activate
      pip install --upgrade salt


---
### Bootstrap EC2 instance

- create a profile for the target in Salt (pillar/servers/hostName)

- run bootstrap

      salt-ssh -i <target> state.sls formula.saltstack.ec2

---

## Salt Prod and Dev environments

running "salt target state.highstate" will run the PROD environment which is always the latest Master branch (synced from Github to saltmaster:/srv/saltstack/prod)

running "saltdev target state.highstate test=true" will run your local hotfix code (located in saltmaster:/srv/saltstack/dev)

if testing local changes to code, sync your Hotfix/working branch changes to saltmaster:/srv/saltstack/dev)

in VSCode, create a new Task (F1 > Tasks:Configure task)

```
{
    // See https://go.microsoft.com/fwlink/?LinkId=733558
    // for the documentation about the tasks.json format
    "version": "2.0.0",
    "tasks": [
        {
            "taskName": "rsync-salt",
            "label": "rsync-salt",
            "type": "shell",
            "command": "rsync -azP salt root@saltmaster:/srv/saltstack/dev/"
        }
    ]
}
```

add a Keyboard shortcut to rsync your local changes to DEV

Preferences > Keyboard shortcut > search for tasks:run task

add a keyboard binding (ie, Alt+R), which will sync your working branch to DEV on saltmaster, you can then test=true your changes using "saltdev" command



salt and saltdev commands are aliases in root's .bashrc

      function salt-dev {
        /usr/bin/salt "${@}" saltenv=dev pillarenv=dev
      }
      function salt-prod {
        /usr/bin/salt "${@}" saltenv=prod pillarenv=prod
      }
      alias saltdev="salt-dev"
      alias salt="salt-prod"


if using Fish, configure functions + sourcing

vim /root/.config/fish/functions

      function salt
        /usr/bin/salt $argv saltenv=prod pillarenv=prod
      end

      function saltdev
        /usr/bin/salt $argv saltenv=dev pillarenv=dev
      end

add to /root/.config/fish/config.fish

      source /root/.config/fish/functions

## Salt Cloud

create 3 new ec2 instances

      salt-cloud -p "profile name" "ec2 name1" "ec2 name2" "ec2 name3"
      salt-cloud -p testbox test1 test2 test3

start/stop/reboot "ec2 name"

      salt-cloud -y -a reboot "ec2 name"

destroy instance

      salt-cloud -d "ec2 name"

show all ec2 instances

      salt-cloud -Q

show cloud providers

      salt-cloud --list-providers

show details on a VM

      salt-cloud -y -a show_instance host1