#!/usr/bin/env python3
## JOBS WATCHER - reports on daily backup, AV and scans jobs.

## WATCHER CLIENT

from __future__ import print_function
import json
import sys
from datetime import datetime
import time
import os

if len(sys.argv) < 4:
    print("not enough arguments: watcher.py SECTION MESSAGE EXIT-CODE")
    sys.exit()

section = sys.argv[1]
message = sys.argv[2]
code = sys.argv[3]

jsonfile = "/etc/salt/watcher.json"


def to_seconds(date):
    """create timestamp for py2, for py3.5+ use datetime.timestamp()"""
    return time.mktime(date.timetuple())


def addval(jsondata):
    timestamp = datetime.now().strftime("%Y-%m-%d %H:%M:%S.%f")
    jsondata[section]["timestamp"] = "{}".format(timestamp)
    jsondata[section]["message"] = message
    jsondata[section]["code"] = code


def update():

    # watcher.json does not exist
    if not os.path.exists(jsonfile):

        # create return dict
        data = {}
        data[section] = {}
        addval(data)

        with open(jsonfile, "w+") as f:
            json.dump(data, f)
            sys.exit()

    # watcher.json already exists
    with open(jsonfile) as f:
        jsondata = json.load(f)

        if jsondata.has_key(section):
            addval(jsondata)
        else:
            jsondata[section] = {}
            addval(jsondata)

    with open(jsonfile, "w") as f:
        json.dump(jsondata, f)


if __name__ == "__main__":
    update()
