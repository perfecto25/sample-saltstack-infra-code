#!/bin/bash

# salt minion installer

export PYTHONIOENCODING=utf8
VENVPATH="/opt/salt"


# get latest py3 version
[ -f /bin/python3 ] && PYPATH=/bin/python3
[ -f /bin/python3.6 ] && PYPATH=/bin/python3.6
[ -f /bin/python3.7 ] && PYPATH=/bin/python3.7
[ -f /bin/python3.8 ] && PYPATH=/bin/python3.8
[ -f /bin/python3.9 ] && PYPATH=/bin/python3.9
[ -f /bin/python3.10 ] && PYPATH=/bin/python3.10

[ -z "${PYPATH}" ] && { echo "No python3 detected, exiting"; exit 1; }


## add your Saltmaster internal IP to hosts
echo "10.1.1.1 saltmaster" >> /etc/hosts

# upgrade pip
$PYPATH -m pip install --upgrade pip --proxy http://<proxyIP>:<proxyPort>

# create venv
[ -d "${VENVPATH}/bin" ] || { cd "/opt"; $PYPATH -m venv salt; }

# install pkgs
[ -f "${VENVPATH}/bin/salt" ] || /opt/salt/bin/pip3 install salt pyinotify dictor --proxy http://<proxyIP>:<proxyPort>

ln -sf $VENVPATH/bin/salt-minion /usr/bin/salt-minion
ln -sf $VENVPATH/bin/salt-call /usr/bin/salt-call


echo "
[Unit]
Description=The Salt Minion
Documentation=man:salt-minion(1) file:///usr/share/doc/salt/html/contents.html https://docs.saltstack.com/en/latest/contents.html
After=network.target salt-master.service

[Service]
KillMode=process
Type=notify
NotifyAccess=all
LimitNOFILE=8192
ExecStart=/opt/salt/bin/salt-minion

[Install]
WantedBy=multi-user.target
" >> /usr/lib/systemd/system/salt-minion.service

systemctl daemon-reload

mkdir /etc/salt

echo "
master: saltmaster
id: $(hostname)
" >> /etc/salt/minion

