f2b_install_pkg:
    pkg.installed:
        - name: fail2ban


f2b_jail_local:
    file.managed:
        - name: /etc/fail2ban/jail.local
        - source: salt://{{ slspath }}/files/jail.local
        - template: jinja 
        - user: root
        - group: root
        - mode: 0644

f2b_service_start:
    service.running:
        - name: fail2ban
        - watch:
            - file: /etc/fail2ban/jail.local
        - require:
            - pkg: fail2ban
