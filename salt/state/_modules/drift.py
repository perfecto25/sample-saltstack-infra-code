import logging
import salt
from salt.exceptions import CommandExecutionError

log = logging.getLogger(__name__)
__outputter__ = {"show": "json"}


def show():
    """
    'drift.show' - shows config drift, ie, number of pending changes - outputs to JSON

    > salt nycweb01 drift.show
    """

    try:
        histate = __salt__["state.highstate"]("test=true")
    except Exception as e:
        raise CommandExecutionError("error running Drift check: %s" % str(e))
    return histate
