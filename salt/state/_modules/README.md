# Custom Salt modules

1. place .py module here
2. run ``` salt 'target' saltutil.sync_all```
3. run new module

    salt 'target' yourmod.func_name


## SUDO
grants temporary sudo access for a user

usage:

    salt host1 sudo.status   // check who currently has sudo access
    salt host1 sudo.grant jsmith 'fixing disk space issue' span=2h  // grants user sudo access, also emails DevOps admins that a sudo access has been given, span lasts 2 hours (or d for days)
    salt host1 sudo.revoke  // revokes all sudo access for the host
    salt host1 sudo.revoke jsmith  // revoke only for this user



