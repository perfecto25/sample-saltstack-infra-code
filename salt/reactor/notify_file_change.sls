inotify_email:
    local.smtp.send_msg:
        - tgt: {{ data['id'] }}
        - kwarg:
            recipient: user@company.com
            message: "WARNING: File: {{ data['path'] }} was modified on host: {{ data['id'] }} on {{ data['_stamp'] }}. \n\n This file was reset to baseline configuration."
            subject: "WARNING: Monitored file was modified outside of Salt."
            profile: salt-smtp

            
