temp_prod_access:
    local.state.sls:
        - tgt: {{ data['id'] }}
        - arg:
            - formula.user.add_key
        - kwarg:
            pillar:
                user: {{ data['data']['user'] }}
                target_user: {{ data['data']['target_user'] }}
        
